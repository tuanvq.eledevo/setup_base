package com.example.controllerapi.controller;

import com.example.controllerapi.RegisterRequest;
import com.example.controllerapi.Response;
import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Data
@RestController
@RequestMapping("/v1")
public class BaseController {
    @Bean
    private RestTemplate restTemplate() {
        return new RestTemplate();
    }
//    @Autowired
//    public BaseController(SendData1 sendData1) {
//        this.sendData1 = sendData1;
//    }

    @PostMapping ("/call-api")
    public ResponseEntity<Response> callAPIFromProject1(@RequestBody RegisterRequest registerRequest) {
        String project2URL = "http://localhost:8082/api/v1/auth/authenticate";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<RegisterRequest> requestEntity = new HttpEntity<>(registerRequest, headers);
        ResponseEntity<Response> response = restTemplate().postForEntity(project2URL, requestEntity, Response.class);
        Response responseBody = response.getBody();
        return ResponseEntity.ok(responseBody);
    }

    @GetMapping("/get-api")
    public ResponseEntity<String> callAPIFromProject1(@RequestHeader("Authorization") String token) {
        System.out.println(token + "check");
        String project2URL = "http://localhost:8082/api/v1/hello/world";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", token);
        HttpEntity<RegisterRequest> requestEntity = new HttpEntity<>(headers);
        try {
            ResponseEntity<String> response = restTemplate().exchange(project2URL, HttpMethod.GET, requestEntity, String.class);
            String responseBody = response.getBody();
            return ResponseEntity.ok(responseBody);
        } catch (HttpClientErrorException.Forbidden ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.FORBIDDEN);
        }
    }

}
