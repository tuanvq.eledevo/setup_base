//package com.example.controllerapi.service;
//
//import com.example.controllerapi.RegisterRequest;
//import com.example.controllerapi.Response;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Component;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//@Component
//@FeignClient(name = "test", url = "https://jsonplaceholder.typicode.com")
//public interface SendData1 {
//    @RequestMapping(method = RequestMethod.GET, value = "/posts", produces = "application/json")
//    String getPosts();
//}