package com.example.controllerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class ControllerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControllerApiApplication.class, args);
	}

}
